<?php

/**
 * @file
 * Admin options for the responsify module.
 */

/**
 * The responsify admin settings form.
 *
 * @see responsify_admin_form_add_device()
 * @see responsify_admin_form_remove_device()
 * @see responsify_admin_form_submit()
 * @see responsify_admin_form_validate()
 */
function responsify_admin_form($form, &$form_state) {

  // We will have many fields with the same name, so we need to be able to access the form hierarchically.
  $form['#tree'] = TRUE;

  // Get saved devices if this is first load and the user hasn't just cleared the last device.
  if (empty($form_state['devices'])
    && !(isset($form_state['cleared_first_row']) && $form_state['cleared_first_row'])) {
    $form_state['devices'] = variable_get('responsify_devices', NULL);
    if (isset($form_state['devices'])) {
      $form_state['devices'] = unserialize($form_state['devices']);
    }
  }

  unset($form_state['cleared_first_row']);

  if (!isset($form_state['dynamic_load'])) {
    $form_state['dynamic_load'] = variable_get('responsify_dynamic_load', FALSE);
  }

  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['general_settings']['dynamic_load'] = array(
    '#type' => 'checkbox',
    '#title' => t('Dynamically load in device libraries'),
    '#description' => t('With this option checked new device libraries will be dynamically loaded in if the browser width changes. Note that this often requires extra code on your part though because there is no way to unload Javascript once it is loaded. To this end the module will trigger a <code>deviceload</code> event on the <code>html</code> element with 3 parameters - <code>device_name[string]</code>, <code>breakpoint_from[int]</code>, <code>breakpoint_to[int]</code>, so that you can listen for the event and choose what to do.'),
    '#default_value' => $form_state['dynamic_load'],
  );

  // Build the number of breakpoint fieldsets indicated by $form_state['devices'].
  for ($i = 0; $i < count($form_state['devices']); $i++) {
    $form['devices'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Breakpoint #@num', array('@num' => $i)),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['devices'][$i]['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Device name'),
      '#description' => t('Enter the device machine name, use (A-Z) (a-z) (0-9) (-) (_) (e.g. mobile).<br />If you used "mobile" you would call your javascript like so: <code>drupal_add_js(\'my_script.js?device=mobile\');</code>'),
      '#size' => 20,
      '#maxlength' => 32,
      '#required' => TRUE,
      '#default_value' => isset($form_state['devices'][$i]['name']) ? $form_state['devices'][$i]['name'] : '',
    );

    $form['devices'][$i]['breakpoint_from'] = array(
      '#type' => 'textfield',
      '#title' => t('Device breakpoint START FROM'),
      '#description' => t('Enter the device breakpoint start value (in pixels, e.g "0").'),
      '#size' => 4,
      '#maxlength' => 4,
      '#required' => TRUE,
      '#default_value' => isset($form_state['devices'][$i]['breakpoint_from']) ? $form_state['devices'][$i]['breakpoint_from'] : '',
    );

    $form['devices'][$i]['breakpoint_to'] = array(
      '#type' => 'textfield',
      '#title' => t('Device breakpoint UP TO'),
      '#description' => t('Enter the device breakpoint end value (in pixels, e.g "480").'),
      '#size' => 4,
      '#maxlength' => 4,
      '#required' => TRUE,
      '#default_value' => isset($form_state['devices'][$i]['breakpoint_to']) ? $form_state['devices'][$i]['breakpoint_to'] : '',
    );

    $form['devices'][$i]['remove'] = array(
      '#type' => 'submit',
      '#value' => 'Remove this Device',
      // Name must be unique if value appears multiple times.
      '#name' => 'edit-device-breakpoint-' . $i . '-remove',
      '#submit' => array('responsify_admin_form_remove_device'),
      '#attributes' => array('data-row' => $i),
      // Since we are removing a device, don't validate until later.
      '#limit_validation_errors' => array(),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

  // Adds an add a device button.
  $form['add_device'] = array(
    '#type' => 'submit',
    '#value' => t('Add another device'),
    '#submit' => array('responsify_admin_form_add_device'),
  );

  if (empty($form_state['devices'])) {
    $form['add_device']['#value'] = t('Add your first device');
    $form['add_device']['#limit_validation_errors'] = array();
  }

  return $form;
}

/**
 * Adds a new device to the device array.
 */
function responsify_admin_form_add_device($form, &$form_state) {

  if (!isset($form_state['devices'])) {
    $form_state['devices'] = array();
  }

  array_push($form_state['devices'], array('name' => ''));
  drupal_set_message(t('Added device: Remember to save your changes.'), 'warning');
  $form_state['rebuild'] = TRUE;
}

/**
 * Removes the clicked device from the device array.
 */
function responsify_admin_form_remove_device($form, &$form_state) {

  if (!empty($form_state['clicked_button']['#attributes'])
    && isset($form_state['clicked_button']['#attributes']['data-row'])) {
    $row = $form_state['clicked_button']['#attributes']['data-row'];

    // array_splice so the numerical indexes are adjusted.
    array_splice($form_state['devices'], $row, 1);

    if ($row == 0) {
      $form_state['cleared_first_row'] = TRUE;
    }

    drupal_set_message(t('Removed device: Remember to save your changes.'), 'warning');
  }
  else {
    form_set_error(NULL, t('Removal error: Could not determine the device.'));
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Validation handler for responsify_admin_form.
 */
function responsify_admin_form_validate($form, &$form_state) {

  for ($i = 0; $i < count($form_state['devices']); $i++) {
    $name = $form_state['values']['devices'][$i]['name'];
    $from = $form_state['values']['devices'][$i]['breakpoint_from'];
    $to = $form_state['values']['devices'][$i]['breakpoint_to'];

    if (empty($name)) {
      form_set_error('devices][$i][name', t('This field is required'));
    }

    if (!isset($from)) {
      form_set_error('devices][$i][breakpoint_from', t('This field is required'));
    }

    if (!isset($to)) {
      form_set_error('devices][$i][breakpoint_to', t('This field is required'));
    }

    if (preg_match('/^[A-Za-z0-9_]+$/', $name) != 1) {
      form_set_error('devices][$i][name', t('Please only enter alphanumeric, underscores and hyphens (A-Z) (a-z) (0-9) (_) (-).'));
    }

    $are_numbers = TRUE;

    if (!ctype_digit($from)) {
      form_set_error('devices][$i][breakpoint_from', t('Please enter a whole number.'));
      $are_numbers = FALSE;
    }

    if (!ctype_digit($to)) {
      form_set_error('devices][$i][breakpoint_to', t('Please enter a whole number.'));
      $are_numbers = FALSE;
    }

    if ($are_numbers && (int) $from >= (int) $to) {
      form_set_error('devices][$i][breakpoint_from', t('The FROM breakpoint should be less than the TO breakpoint.'));
    }
  }
}

/**
 * Submit handler for responsify_admin_form.
 */
function responsify_admin_form_submit($form, &$form_state) {

  variable_set('responsify_dynamic_load', $form_state['values']['general_settings']['dynamic_load']);

  if (!empty($form_state['values']['devices'])) {
    variable_set('responsify_devices', serialize($form_state['values']['devices']));
  }

  drupal_set_message(t('Responsify settings have been saved.'));
}
