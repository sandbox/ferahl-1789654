(function($) {

  $(document).ready(function() {
    if(Drupal.settings.responsify.dynamic_load) {
      $(window).resize(loadJS);
    }

    loadJS();
  });

  function getURIParameter(uri, name) {
    return decodeURI(
      (RegExp(name + '=' + '(.+?)(&|$)').exec(uri)||[,null])[1]
    );
  }

  function loadJS() {
    var wndWidth = $(window).width();
    var devicesToLoad = [];
    var libsToLoad = [];
    var libsToLoadAsync = [];

    // Find devices in this width that haven't been loaded yet.
    $.each(Drupal.settings.responsify.devices, function(i, v) {
      if(wndWidth >= v.breakpoint_from && wndWidth <= v.breakpoint_to) {
        $('html').trigger('deviceload', [v.name, v.breakpoint_from, v.breakpoint_to]);
        if(!v.loaded) {
          devicesToLoad.push(v);
          $.extend(Drupal.settings.responsify.devices[i], {loaded: true});
        }
      }
    });

    if(devicesToLoad.length === 0) { return; }

    // Find device specific libraries for loading.
    $.each(Drupal.settings.responsify.libraries, function(i, v) {
      var device = getURIParameter(v, 'device');

      $.each(devicesToLoad, function(i2, v2) {
        if(device === v2.name) {

          // Load sequentially or async?
          var async = getURIParameter(v, 'async');
          async = (async === 'true');

          if(async === true) {
            libsToLoadAsync.push(v);
          }
          else {
            libsToLoad.push(v);
          }
        }
      });

    });

    if(libsToLoad.length === 0 && libsToLoadAsync.length === 0) { return; }

    // Save a copy of Drupal.behaviors so we can execute new ones after
    // we load in the new libraries.
    var origBehaviors = $.extend({}, Drupal.behaviors);

    // Load any asyc libraries without waiting for them to return
    _loadRecursive(libsToLoadAsync, 0, true);

    // Once code has executed load the sequential ones
    _loadRecursive(libsToLoad, 0, false);

    // This function will load in the libs either async or sync
    function _loadRecursive(libs, index, async) {

      if(!libs.length || index >= libs.length) { return; }

      $.ajax({
        url: libs[index],
        dataType: 'script',
        complete: function(jqXHR, textStatus) {

          // Execute new behaviors as they come for the fastest
          // performance and easiest guarantee of correct load order
          // for sequential loading.
          _execNewBehaviors();

          if(async === false) {
            _loadRecursive(libs, index + 1, async);
          }
        }
      });

      if(async === true) {
        _loadRecursive(libs, index + 1, async);
      }
    }

    function _execNewBehaviors() {

      $.each(Drupal.behaviors, function(k, obj) {
        if(!origBehaviors[k]) {
          if($.isFunction(obj.attach)) { obj.attach(document, Drupal.settings); }
           origBehaviors[k] = obj;
        }
      });
    }
  }

})(jQuery);
