-- SUMMARY --

Responsify allows you to set up devices which have FROM and TO pixel breakpoints.
You can use these devices in drupal_add_js calls to load in different JavaScript 
depending on whether the user is in the device bounds you specified.

For a full description of the module, visit the project page:
http://drupal.org/project/responsify

submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/responsify

-- INSTALLATION --

Simply enable the module and you are good to go after adding your devices
(see Configuration).

-- CONFIGURATION --

1. Give permissions to edit the settings page to those roles you want
2. Set up devices on the settings page at admin/settings/responsify

-- PERFORMANCE TIPS --
It is best to load the bulk of your scripts normally to avoid visual delays.
By default Responsify will load your scripts sequentially - that is to say it 
will wait for a script to return from the server before loading the next. If 
your script however does not depend on others that are loaded with the device 
arg then you can add the argument async=true to your drupal_add_js call and 
these will load without waiting for each other to complete.

-- FAQ --

Q: How do I stop unwanted code from running when I enter a new device width?
A: There is no way to unload javascript, this is why the module triggers events
on the html element so you can know when a new device is going to load and act
accordingly. Listen to the event like so: 
$('html').bind('deviceload', function(ev, device_name, breakpoint_from, breakpoint_to) {...});

-- CONTACT --

Current maintainers:
* Dominic Tobias (ferahl) - http://drupal.org/user/1618294
